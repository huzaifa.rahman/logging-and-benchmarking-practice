#include<iostream>
#include "spdlog/spdlog.h"
#include "NanoLogCpp17.h"
#include "timer.h"
#include <vector>

int main()
{
    timer _timer;
    std::vector<int> my_vect;
    NanoLog::setLogFile("../utils/performance-logs");
    NanoLog::preallocate();
    NanoLog::setLogLevel(NanoLog::NOTICE);

    for(int i = 0; i < 10000; ++i)
    {
        _timer.start();
        for(int i = 0; i < 10000; ++i)
        {
            my_vect.push_back(i);
        }
        _timer.stop();
        double latency = _timer.get_elapsed_ns();
        NANO_LOG(NOTICE, "Duration: *%0.0f", latency);
    }
        
    spdlog::info("Hello, {}!", "World");
}
