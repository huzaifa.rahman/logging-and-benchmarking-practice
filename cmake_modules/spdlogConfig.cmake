include(ExternalProject)

if(NOT TARGET spdlog_project)
ExternalProject_Add(spdlog_project 
    GIT_REPOSITORY https://github.com/gabime/spdlog.git
    GIT_TAG v1.5.0
    CMAKE_ARGS 
              -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/spdlog 
              -DCMAKE_BUILD_TYPE=Release
              -DSPDLOG_BUILD_EXAMPLE=OFF 
              -DSPDLOG_BUILD_EXAMPLE_HO=OFF 
              -DSPDLOG_BUILD_TESTS=OFF 
              -DSPDLOG_BUILD_TESTS_HO=OFF
)
endif()

ExternalProject_Get_Property(spdlog_project BINARY_DIR)
ExternalProject_Get_Property(spdlog_project SOURCE_DIR)

set(spdlog_INCLUDE_DIR ${SOURCE_DIR}/include) 
set(spdlog_LIB_DIR ${BINARY_DIR}) 

add_library(spdlog STATIC IMPORTED)

set_property(TARGET spdlog PROPERTY IMPORTED_LOCATION ${spdlog_LIB_DIR}/libspdlog.a)
add_dependencies(spdlog spdlog_project)
install(FILES ${spdlog_LIB_DIR}/libspdlog.a DESTINATION lib)

